package saiflimited.com.smsblast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by huzefagadi on 14/02/18.
 */

public class Campaign {

    @SerializedName("sms_group_id")
    public int smsGroupId;

    @SerializedName("camphana")
    public String campaign;

    @SerializedName("message")
    public String message;

    @Override
    public String toString() {
        return campaign;
    }
}
