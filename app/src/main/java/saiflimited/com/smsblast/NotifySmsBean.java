package saiflimited.com.smsblast;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by huzefagadi on 15/02/18.
 */

public class NotifySmsBean {

    @SerializedName("sms_group_id")
    public int smsGroupId;

    @SerializedName("sent_info")
    public List<String> sentInfo;
}
