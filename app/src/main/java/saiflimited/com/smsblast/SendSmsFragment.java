package saiflimited.com.smsblast;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SendSmsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SendSmsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Api mApi;

    @BindView(R.id.pageLayout)
    LinearLayout mPageLayout;

    @BindView(R.id.progressbar)
    ProgressBar mProgressBar;

    @BindView(R.id.playPauseToggleButton)
    ToggleButton playPauseToggleButton;

    @BindView(R.id.campaignSpinner)
    AppCompatSpinner mCampaignSpinner;

    @BindView(R.id.unitSpinner)
    AppCompatSpinner mFrequencyUnit;

    //private ProgressDialog mProgress;
    private List<MobileNumber> mMobileNumbers;
    private String mMessage;

    @BindView(R.id.result)
    TextView mResult;

    @BindView(R.id.messageFrequency)
    TextView mMessageFrequency;

    @BindView(R.id.messageCount)
    TextView mMessageCount;

    int mInitialCount = 0;
    int mSmsGroupId;
    Disposable mDisposableForTimer;

    public SendSmsFragment() {
        // Required empty public constructor
    }

    public void setApi(Api api) {
        mApi = api;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_send_sms, container, false);
        ButterKnife.bind(this, rootView);
        showProgress();
        initializeWidgets();
        getCampaigns();
        return rootView;
    }

    private void getCampaigns() {
        Observable<List<Campaign>> observable = mApi.listCampaings();
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Campaign>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(List<Campaign> value) {
                        Log.d("values", value.get(0).message);
                        hideProgress();
                        Campaign select = new Campaign();
                        select.campaign = "Select Campaign";
                        value.add(0, select);
                        CustomSpinnerAdapter<Campaign> adapterForTransaction = new CustomSpinnerAdapter<Campaign>(
                                getContext(), R.layout.custom_auto_complete_dropdown, value);

                        adapterForTransaction.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        mCampaignSpinner.setAdapter(adapterForTransaction);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void initializeWidgets() {
       /* mCampaignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });*/
        mCampaignSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                mInitialCount = 0;
                if (position > 0) {
                    mMobileNumbers = null;
                    mMessage = ((Campaign) parentView.getSelectedItem()).message;
                    showProgress();
                    getNumbersForGroup(((Campaign) parentView.getSelectedItem()).smsGroupId);
                    mSmsGroupId = ((Campaign) parentView.getSelectedItem()).smsGroupId;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        playPauseToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Log.d("toggle", "checked");
                    showProgress();
                    startMessageProcessing();
                } else {
                    Log.d("toggle", "not checked");
                    hideProgress();
                    stopMessageProcessing();
                }
            }
        });
    }


    private void resetInfo() {
        mMobileNumbers = null;
        mInitialCount = 0;
    }

    private void getNumbersForGroup(int groupId) {
        Observable<List<MobileNumber>> observable = mApi.getNumbersForGroup(groupId);
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MobileNumber>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(List<MobileNumber> value) {
                        hideProgress();
                        mMobileNumbers = value;
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        hideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @OnClick(R.id.submitButton)
    public void onClick(View v) {


        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "Please provide all the permission in settings for this app", Toast.LENGTH_LONG).show();
            return;
        }

        mResult.setVisibility(View.INVISIBLE);
        mInitialCount = 0;
        if (mMessageFrequency.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please add message frequency", Toast.LENGTH_LONG).show();
            return;
        }

        if (mMessageCount.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Please add message count", Toast.LENGTH_LONG).show();
            return;
        }
        if(!playPauseToggleButton.isChecked()){
            playPauseToggleButton.setChecked(true);
        }
    }

    private void stopMessageProcessing() {
        if (mDisposableForTimer != null && !mDisposableForTimer.isDisposed()) {
            mDisposableForTimer.dispose();
        }
    }
    private void startMessageProcessing() {
        final int messageCount = Integer.parseInt(mMessageCount.getText().toString());
        int messageFrequency = Integer.parseInt(mMessageFrequency.getText().toString());
        int frequencyUnit = mFrequencyUnit.getSelectedItemPosition();

        if (frequencyUnit == 0) {
            messageFrequency = messageFrequency * 60 * 1000;
        } else {
            messageFrequency = messageFrequency * 1000;
        }

        Observable.interval(messageFrequency, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposableForTimer = d;
                    }

                    @Override
                    public void onNext(Long value) {
                        List<String> numbers = new ArrayList<>();
                        if ((mInitialCount + messageCount) < mMobileNumbers.size()) {
                            for (int i = 0; i < messageCount; i++) {
                                String message = mMessage.replace("[name]", mMobileNumbers.get(mInitialCount).name);
                                message = message.replace("[number]", mMobileNumbers.get(mInitialCount).mobileNumber);
                                sendSMS(mMobileNumbers.get(mInitialCount).mobileNumber, message);
                                numbers.add(mMobileNumbers.get(mInitialCount).mobileNumber);
                                mInitialCount++;
                            }
                        } else if (mInitialCount != mMobileNumbers.size()) {
                            for (; mInitialCount < mMobileNumbers.size(); mInitialCount++) {
                                String message = mMessage.replace("[name]", mMobileNumbers.get(mInitialCount).name);
                                message = message.replace("[number]", mMobileNumbers.get(mInitialCount).mobileNumber);
                                sendSMS(mMobileNumbers.get(mInitialCount).mobileNumber, message);
                                numbers.add(mMobileNumbers.get(mInitialCount).mobileNumber);
                            }
                        } else {
                            stopMessageProcessing();
                            hideProgress();
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    mResult.setText("Successfull");
                                    mResult.setTextColor(Color.GREEN);
                                    mResult.setVisibility(View.VISIBLE);
                                    getCampaigns();
                                }
                            });

                        }

                        if (!numbers.isEmpty()) {
                            notifySmsSent(numbers);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                mResult.setText("Failed");
                                mResult.setTextColor(Color.RED);
                                mResult.setVisibility(View.VISIBLE);
                                getCampaigns();
                            }
                        });

                        stopMessageProcessing();
                    }

                    @Override
                    public void onComplete() {
                        hideProgress();
                    }
                });
    }


    private void notifySmsSent(List<String> array) {
        NotifySmsBean notifySmsBean = new NotifySmsBean();
        notifySmsBean.sentInfo = array;
        notifySmsBean.smsGroupId = mSmsGroupId;
        Log.d("request", new Gson().toJson(notifySmsBean));
        Observable<ApiResponse> observable = mApi.notifySmsSent(notifySmsBean);
        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResponse value) {
                        Log.d("Response", new Gson().toJson(value));
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void sendSMS(String phoneNo, String msg) {
        Log.d("sent sms", "sms sent to " + phoneNo);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void showProgress() {
       /* if (mProgress == null) {
            mProgress = new ProgressDialog(getContext());
            mProgress.setCancelable(false);
            mProgress.setTitle("Processing");
            mProgress.setMessage("Please wait..");
        }

        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                mProgress.show();
            }
        });*/

        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                enableLayout(false);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void hideProgress() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                enableLayout(true);
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    private void enableLayout(boolean enable) {
        for ( int i = 0; i < mPageLayout.getChildCount();  i++ ){
            View view = mPageLayout.getChildAt(i);
            view.setEnabled(enable); // Or whatever you want to do with the view.
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
