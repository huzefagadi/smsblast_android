package saiflimited.com.smsblast;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by huzefagadi on 14/02/18.
 */

public interface Api {


    @GET("ws_campain_list.php")
    Observable<List<Campaign>> listCampaings();

    @GET("getNumbersForGroup.php")
    Observable<List<MobileNumber>> getNumbersForGroup(@Query("gid") int smsGroupId);

    @POST("notifySmsSent.php")
    Observable<ApiResponse> notifySmsSent(@Body Object body);


}